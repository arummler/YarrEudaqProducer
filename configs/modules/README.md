moduleType: ITk_single, ITk_quad, ITk_triplet (spelling important; used in converter plugin)
pcbType: ITk_RD53A_SCC, ITk_RD53A_quad_v3.8, ITk_RD53B_quad_v?.?
sensorType: ITk_RD53A_single, ITk_RD53A_quad, ITk_RD53B_single, ITk_RD53B_quad
moduleID: e.g. 20UPGM20124046
